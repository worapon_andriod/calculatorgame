package com.example.calculatorgame

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.example.calculatorgame.MenuActivity.Companion.pointCorrect
import com.example.calculatorgame.MenuActivity.Companion.pointIncorrect
import com.example.calculatorgame.MenuActivity.Companion.sign
import kotlin.random.Random

class PlusActivity : AppCompatActivity() {

    override fun onRestart() {
        super.onRestart()
        setPoint()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_plus)

        setPoint()
        play()

    }

    private fun play() {
        val result = setQuestion()
        val btn1 = findViewById<Button>(R.id.btn1)
        val btn2 = findViewById<Button>(R.id.btn2)
        val btn3 = findViewById<Button>(R.id.btn3)
        val txtAnswer = findViewById<TextView>(R.id.txtAnswer)
        randomButton(result, btn1, btn2, btn3)

        this.checkClick(btn1, result, txtAnswer)
        this.checkClick(btn2, result, txtAnswer)
        this.checkClick(btn3, result, txtAnswer)
    }


    private fun checkClick(btn: Button, result: Int, txtAnswer: TextView) {
        btn.setOnClickListener {
            if (btn.text.toString().toInt() == result) {
                ansCorrect(txtAnswer)
            } else {
                ansIncorrect(txtAnswer)
            }
            setPoint()
            play()
        }
    }

    private fun randomButton(result: Int, btn1: Button, btn2: Button, btn3: Button) {
        val randomNum = Random.nextInt(1, 4)
        val btnValue1 = (result + 1).toString()
        val btnValue2 = (result - 1).toString()
        if (randomNum == 1) {
            btn1.text = result.toString()
            btn2.text = btnValue1
            btn3.text = btnValue2

        } else if (randomNum == 2) {
            btn1.text = btnValue1
            btn2.text = result.toString()
            btn3.text = btnValue2
        } else {
            btn1.text = btnValue1
            btn2.text = btnValue2
            btn3.text = result.toString()
        }
    }

    private fun setQuestion(): Int {

        val txtSign = findViewById<TextView>(R.id.txtSign)
        val num1 = findViewById<TextView>(R.id.num1)
        val num2 = findViewById<TextView>(R.id.num2)

        val number1 = Random.nextInt(0, 10)
        val number2 = Random.nextInt(0, 10)

        if (sign.equals("+")){
            val result = number1 + number2
            txtSign.text = "+"
            num1.text = number1.toString()
            num2.text = number2.toString()
            return result
        }else if (sign.equals("-")){
            val result = number1 - number2
            txtSign.text = "-"
            num1.text = number1.toString()
            num2.text = number2.toString()
            return result
        }else {
            val result = number1 * number2
            txtSign.text = "*"
            num1.text = number1.toString()
            num2.text = number2.toString()
            return result
        }

    }



    private fun setPoint() {
        var txtPointCorrect = findViewById<TextView>(R.id.txtPointCorrect)
        var txtPointIncorrect = findViewById<TextView>(R.id.txtPointIncorrect)
        txtPointCorrect.text = pointCorrect.toString()
        txtPointIncorrect.text = pointIncorrect.toString()
    }

    private fun ansIncorrect(txtAnswer: TextView) {
        txtAnswer.text = ("ผิด")
        txtAnswer.setTextColor(Color.parseColor("#ff0000"))
        pointIncorrect+=1
    }

    private fun ansCorrect(txtAnswer: TextView) {
        txtAnswer.text = ("ถูก")
        txtAnswer.setTextColor(Color.parseColor("#00ff00"))
        pointCorrect+=1
    }


}