package com.example.calculatorgame

import android.content.IntentFilter
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import org.w3c.dom.Text

class MenuActivity : AppCompatActivity() {

    companion object{
        var sign = ""
        var pointCorrect = 0
        var pointIncorrect = 0

    }

    override fun onRestart() {
        super.onRestart()
        setMenuPoint()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)



        val btnPlus = findViewById<Button>(R.id.btnPlus)
        btnPlus.setOnClickListener {
            val intent = Intent(MenuActivity@this, PlusActivity::class.java)
            startActivity(intent)
            sign = "+"
        }


        val btnMinus = findViewById<Button>(R.id.btnMinus)
        btnMinus.setOnClickListener {
            val intent = Intent(MenuActivity@this, PlusActivity::class.java)
            startActivity(intent)
            sign = "-"
        }
        val btnMulti = findViewById<Button>(R.id.btnMulti)
        btnMulti.setOnClickListener {
            val intent = Intent(MenuActivity@this, PlusActivity::class.java)
            startActivity(intent)
            sign = "*"
        }



    }

    private fun setMenuPoint() {
        val txtMenuPointIncorrect = findViewById<TextView>(R.id.txtMenuPointIncorrect)
        val txtMenuPointCorrect = findViewById<TextView>(R.id.txtMenuPointCorrect)
        txtMenuPointCorrect.text = pointCorrect.toString()
        txtMenuPointIncorrect.text = pointIncorrect.toString()
    }
}